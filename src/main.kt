abstract class Person(var name: String, var surname: String, var gpa: Double) {

    constructor(name: String, surname: String) : this(name, surname, 0.0) {

    }

    abstract fun goodBoy(): Boolean
    open fun getDetail(): String {
        return "$name $surname has score $gpa"
    }
}

class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
    override fun goodBoy(): Boolean {
        return gpa > 2.0
    }
    override fun getDetail(): String {
        return "$name $surname has score $gpa and study in $department"
    }
}

fun main(args: Array<String>) {
    val no1: Student = Student("Sonchai", "Pitak", 2.00, "MMIT")
    val no2: Student = Student("Sunchild", "Moonfather",3.00,"Ani")
//    val no3: Person = Person(surname = "Marry", name = "Christmas")
    println(no1.getDetail())
    println(no1.goodBoy())
    println(no2.getDetail())
    println(no2.goodBoy())
//    println(no2.getDetail())
//    println(no3.getDetail())
}
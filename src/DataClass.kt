data class Teacher(var name: String, var courseName: String,var shirtColor: Color)

enum class Color {
    RED, GREEN, BLUE
}

fun main(args: Array<String>) {
    var teacher1: Teacher = Teacher("Somsak", "Democratic", Color.GREEN)
    var teacher2: Teacher = Teacher("Somsak", "Democratic", Color.BLUE)
    var teacher3: Teacher = Teacher("Somnuk", "Mathmatics", Color.GREEN)
    println(teacher1.equals(teacher3))
    println(teacher1.equals(teacher2))
    println(teacher3.courseName)
    println(teacher3.name)
    println(teacher1.component1())
    println(teacher1.component2())
    val (teacherName, teacherCourseName) = teacher3
    println("$teacherName teaches $teacherCourseName")
    println(teacher1)
    println(teacher2)
    println(teacher3)

    var adHoc = object {
        var x :Int =0
        var y :Int = 1
    }
    println(adHoc)
    println(adHoc.x + adHoc.y)
}


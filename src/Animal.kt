abstract class animal(var noOfLegs: Int, var food: String) {
    abstract fun getSound(): String
}

class dog(noOfLegs: Int, food: String, var name: String) : animal(noOfLegs, food) {
    override fun getSound(): String {
        return "Dog Have $noOfLegs leg(s) eat $food  name $name and sound is box"
    }
}

class lion(noOfLegs: Int, food: String) : animal(noOfLegs, food) {
    override fun getSound(): String {
        return "Lion Have $noOfLegs leg(s) eat $food and sound is Wo Hooooooo"
    }
}

fun main(args: Array<String>) {
    val animal1:dog = dog (4,"petdgree","Big")
    val animal2:lion = lion (4,"meat")
    println(animal1.getSound())
    println(animal2.getSound())
}
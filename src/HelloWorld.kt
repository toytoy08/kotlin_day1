fun main(args: Array<String>) {
//    helloMore("Prayuth")
//    helloMore("Sanchai")
//    getName("Firstname", "Lastname")
//    gradeReport("Somchai", 3.33)
//    gradeReport(name = "Nobita")
//    gradeReport(gpa = 2.50)
//    gradeReport()
//    getName("A", "B")
//    getName()
//    println(isOdd(2))
//    println(callSumGrade(80))
//    println(callSumGrade(200))
    var arrays = arrayOf (5,55,200,1,3,5,7)
    var max = findMaxValue(arrays)
    println("max value is $max")
}

fun helloWorld(): Unit {
    println("hello World")
}

fun helloMore(text: String): Unit {
    println("hello $text")
}

fun getName(name: String = "annonymous", surname: String = "Kid"): Unit {
    gradeReport("$surname $name", 2.34)
}

fun gradeReport(name: String = "annonymous", gpa: Double = 0.00): Unit =
    println("mister $name  gpa : is $gpa")

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even value"
    } else {
        return "$value is odd value"
    }
}

fun getAbbreviation(abbr: Char?): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("not smart")
            return "cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 70..80 -> grade = "B"
        in 80..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun callSumGrade(score: Int): String? {
    var result: String? = getGrade(score)
    if (result == null) {
        return "Grade is invalid"
    } else {
        return "Your Grade is $result mean " + getAbbreviation(result.toCharArray()[0])
    }
}

fun loopSimple(args: Array<String>) {
    for (i in 1..3) {
        println(i)
    }
}

fun loopDownTo(args: Array<String>) {
    for (i in 6 downTo 0 step 2) {
        println(i)
    }
}

fun loopArray(args: Array<String>) {
    var arrays = arrayOf(1, 3, 5, 7)
    for (i in arrays.indices) {
        println(arrays[i])
    }
}

fun loopWithIndex(args: Array<String>) {
    var arrays = arrayOf(1, 3, 5, 7)
    for ((index, value) in arrays.withIndex()) {
        println("$index. value = $value")
    }
}

fun findMaxValue(values: Array<Int>): Int {
    var max:Int = values[0]
    for (i in values.indices){
        if (values[i] > max){
            max = values[i]
        }
    }
    return max
}